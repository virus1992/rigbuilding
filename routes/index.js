var express = require('express');
var router = express.Router();
var constants = require('../modules/constants');

/* GET home page. */
router.get('/', function (req, res, next) {
    var params = {title: 'Express'};
    res.render('index', params);
});

module.exports = router;
