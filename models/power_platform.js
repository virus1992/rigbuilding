/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var PowerPlatformSchema = new mongoose.Schema({
    name: {type: String, required: false, default: ""},
    part: {type: String, required: false},
    ourCost: {type: String, required: false},
    wt: {type: String, required: false},
    kW: {type: String, required: false},
    cfm: {type: String, required: false, default: ""},
    laborHours: {type: String, required: false, default: ""},
    platformType: {type: String, required: false, default: ""},
    subPlatformType: {type: String, required: false, default: ""},
    phase: {type: String, required: false, default: ""},
    comboType: {type: String, required: false, default: ""},
    picture: {type: String, required: false, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'power_platform'
});
PowerPlatformSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('power_platform', PowerPlatformSchema);


