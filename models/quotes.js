/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var QuotesSchema = new mongoose.Schema({
    companyName: {type: String, default: ""},
    contactName: {type: String, default: ""},
    address: {type: String, default: ""},
    city: {type: String, default: ""},
    state: {type: String, default: ""},
    country: {type: String, default: ""},
    zipCode: {type: String, default: ""},
    phone: {type: String, default: ""},
    email: {type: String, default: ""},
    desiredRISMargin: {type: String, default: ""},
    salesRepName: {type: String, default: ""},
    repPhone: {type: String, default: ""},
    repEmail: {type: String, default: ""},
    date: {type: String, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'quotes'
});
QuotesSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('quotes', QuotesSchema);


