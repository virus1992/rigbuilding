/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var TruckSizeSchema = new mongoose.Schema({
    truckSize: {type: Number, default: ""},
    gvwRange: {type: String, default: ""},
    payload: {type: String, default: ""},
    boxWeight: {type: String, default: ""},
    height: {type: String, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'truck_sizes'
});
TruckSizeSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('truck_sizes', TruckSizeSchema);


