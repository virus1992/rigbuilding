/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var ConstantsSchema = new mongoose.Schema({
    name: {type: Number, default: ""},
    rate: {type: String, default: ""},
    rateType: {type: String, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'constants'
});
ConstantsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('constants', ConstantsSchema);


