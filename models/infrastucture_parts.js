/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var InfrastucturePartsSchema = new mongoose.Schema({
    infraType: {type: String, default: ""},
    name: {type: String, default: ""},
    part: {type: String, default: ""},
    ourCost: {type: String, default: ""},
    wt: {type: String, default: ""},
    gvw: {type: String, default: ""},
    cfm: {type: Number, default: ""},
    laborHours: {type: Number, default: ""},
    picture: {type: Number, default: ""},
    brochure: {type: Number, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'infrastucture_parts'
});
InfrastucturePartsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('infrastucture_parts', InfrastucturePartsSchema);


