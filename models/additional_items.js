/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var AdditionalItemsSchema = new mongoose.Schema({
    additionalType: {type: String, default: ""},
    name: {type: String, default: ""},
    part: {type: String, default: ""},
    cfm: {type: String, default: ""},
    laborHours: {type: String, default: ""},
    picture: {type: String, default: ""},
    brand: {type: String, default: ""},
    ourCost: {type: String, default: ""},
    wt: {type: String, default: ""},
    kW: {type: String, default: ""},
    brochure: {type: String, default: ""},
    manuals: {type: String, default: ""},
    amps: {type: String, default: ""},
    graco: {type: String, default: ""},
    faceMaskTyoe: [{type: String, default: ""}],
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'additional_items'
});
AdditionalItemsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('additional_items', AdditionalItemsSchema);


