/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var PlatformsSchema = new mongoose.Schema({
    name: {type: String, required: true},
    slug: {type: String, default: false, default: ""},
    isdeleted: {type: Boolean, default: false},
    deletedAt: {type: Number, default: _.now()},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'platforms'
});
PlatformsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('platforms', PlatformsSchema);


