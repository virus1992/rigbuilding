/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var ProportionerPartsSchema = new mongoose.Schema({
    proportionerType: {type: String, default: ""},
    name: {type: String, default: ""},
    part: {type: String, default: ""},
    ourCost: {type: String, default: ""},
    lb: {type: String, default: ""},
    kW: {type: String, default: ""},
    cfm: {type: String, default: ""},
    laborHours: {type: Number, default: ""},
    companyName: [{type: String, default: ""}],
    picture: {type: String, default: ""},
    brochure: {type: String, default: ""},
    partType: {type: String, default: ""},
    brandType: {type: String, default: ""},
    generatorType: [{type: String, default: ""}],
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
        collection: 'proportioner_parts'
});
ProportionerPartsSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('proportioner_parts', ProportionerPartsSchema);


