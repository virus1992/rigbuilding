/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var TrailersSchema = new mongoose.Schema({
    name: {type: String, default: ""},
    length: {type: String, default: ""},
    ourCOst: {type: Number, default: ""},
    wt: {type: Number, default: ""},
    gvw: {type: Number, default: ""},
    cfm: {type: Number, default: ""},
    laborHours: {type: Number, default: ""},
    trailerType: {type: String, default: ""},
    brochure: {type: String, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'trailers'
});
TrailersSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('trailers', TrailersSchema);


