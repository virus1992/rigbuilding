/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var ProportioningSystemSchema = new mongoose.Schema({
    proportioner: {type: String, required: false, default: ""},
    part: {type: String, required: false, default: ""},
    ourCost: {type: String, required: false, default: ""},
    lb: {type: String, required: false, default: ""},
    kW: {type: String, required: false, default: ""},
    mixingChamberCapacity: {type: String, required: false, default: ""},
    laborHours: {type: String, required: false, default: ""},
    powerPlatforms: [{type: String, required: false, default: ""}],
    phase: {type: String, required: false, default: ""},
    picture: {type: String, required: false, default: ""},
    brochure: {type: String, required: false, default: ""},
    proportionerType: {type: String, required: false, default: ""},
    brandType: {type: String, required: false, default: ""},
    hoseLengthCompatibility: {type: String, required: false, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'proportioning_system'
});
ProportioningSystemSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('proportioning_system', ProportioningSystemSchema);


