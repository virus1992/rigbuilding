/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
var _ = require('lodash');
var Schema = mongoose.Schema;
var MixingChamberSchema = new mongoose.Schema({
    size: {type: Number, default: ""},
    cfm: {type: String, default: ""},
    stickPumpAirIncrease: {type: Number, default: ""},
    brochure: {type: Number, default: ""},
    picture: {type: String, default: ""},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Number, default: _.now()},
    updatedAt: {type: Number, default: _.now()}
}, {
    collection: 'mixing_chamber'
});
MixingChamberSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('mixing_chamber', MixingChamberSchema);


